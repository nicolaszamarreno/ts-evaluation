```
  ______      __           __  _____       ______
 /_  __/___ _/ /__  ____  / /_/ ___/____  / __/ /_
  / / / __ `/ / _ \/ __ \/ __/\__ \/ __ \/ /_/ __/
 / / / /_/ / /  __/ / / / /_ ___/ / /_/ / __/ /_  
/_/  \__,_/_/\___/_/ /_/\__//____/\____/_/  \__/  
```

# Evaluation Project

_You enter in a awesome project who permit to evaluate one of your collaborator. You can attribute him objectives and stats._

## Content

To guide you, you can follow these links with the differents things that you need know.

* [You work with VS Code ?](#you-work-with-vs-code)
* [Folder Structure Conventions](#folder-structure-conventions)
* [List of Commands Line](#list-of-commands-line)
* [Tools for development](#tools-for-development)
* [List of Differents Packages](#list-of-differents-packages)

## You work with VS Code ?

If you work with Visual Studio Code, you can install these packages for improve your workflow:

* [CSScomb](https://marketplace.visualstudio.com/items?itemName=mrmlnc.vscode-csscomb)
* [Prettier - Code formatter](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
* [TSLint](https://marketplace.visualstudio.com/items?itemName=eg2.tslint)

## Folder Structure Conventions

This convention is the temporary organisation.

```
.
├── dist
│   ├── fonts
│   ├── bundle.js
│   ├── index.html
│   └── style.css
├── src
│   ├── assets
│       ├── js
│       └── less
│   ├── components
│       ├── atoms
│       ├── molecules
│       ├── organisms
│       ├── pages
│       ├── settings
│       └── templates
│   ├── containers
│   ├── interfaces
│   ├── pages
│   ├── index.tsx
│   └── store.ts
├── .csscomb.json
├── .gitignore
├── .prettierrc
├── package-lock.json
├── package.json
├── postcss.config.js
├── tsconfig.json
├── tslint.json
├── webpack.config.json
├── yarn.lock
├── README.md
└── ...
```

## List of Commands Line

For launch watch and compilation of the files, you can pass:

**Via yarn**

```bash
$ yarn run dev
```

**Via NPM**

```bash
$ npm run dev
```

## Tools for the development

* Typescript
* Prettier
* TSLint
* CSSComb

## List of principales packages

* Webpack
* React
* MobX
