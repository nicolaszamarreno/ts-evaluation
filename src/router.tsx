import * as React from "react"
import { Switch, Route } from "react-router"
import { Objectives } from "./views/objectives/objectives"
import { Progress } from "./views/progress/progress"

const configRouterLinks = [
    {
        name: "Objectives",
        path: "/"
    },
    {
        name: "My Profile",
        path: "/my-profile"
    },
    {
        name: "Job Description",
        path: "/job-description"
    },
    {
        name: "Organisation chart",
        path: "/organisation-chart"
    },
    {
        name: "Log",
        path: "/log"
    },
    {
        name: "Salary",
        path: "/salary"
    }
]

class Router extends React.Component<{}, {}> {
    render() {
        return (
            <Switch>
                <Route exact path="/" component={Objectives} />
                <Route path="/my-profile" component={Progress} />
            </Switch>
        )
    }
}

export { Router, configRouterLinks }
