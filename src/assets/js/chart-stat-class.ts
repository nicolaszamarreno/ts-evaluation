import { Colors } from "../../interfaces/atoms/chart-bar"

export interface Colors {
    good: string
    medium: string
    bad: string
}

export interface OptionsChart {
    heightContainer?: number
    heightBar?: number
    paddingCornerRound?: number
    speed?: number
    fontSize?: number
    displayLegend?: boolean
    colors?: Colors
    colorsStates?: boolean
    animOnScroll?: boolean
    percent: number
}

export default class ChartStatClass {
    private $chart: any
    private ctx: any
    private animation: any
    private heightCanvas: number
    private widthCanvas: number
    private heightContainer: number
    private heightBar: number
    private width: number
    private paddingCornerRound: number
    private speed: number
    private fontSize: number
    private start: number
    private colorsStates: boolean
    private colors: Colors
    private animOnScroll: boolean
    private isAlreadyAnimated: boolean
    private percentMax: number
    private displayLegend: boolean

    constructor($element: HTMLCanvasElement, options: OptionsChart) {
        this.$chart = $element
        this.heightContainer = options.heightContainer || 20
        this.heightBar = options.heightBar || 15
        this.width = this.$chart.offsetWidth
        this.paddingCornerRound = options.paddingCornerRound || 8
        this.speed = options.speed || 1
        this.fontSize = options.fontSize || 12
        this.displayLegend = options.displayLegend || true
        this.start = 0

        //Set options
        this.colorsStates = options.colorsStates || false
        this.colors = { good: "#308649", medium: "#FD9141", bad: "#D11548" } || options.colors
        this.animOnScroll = options.animOnScroll || false

        if (this.animOnScroll) {
            this.isAlreadyAnimated = false
        }

        this.setSizeChart()
        this.createChart()

        this.percentMax = this.calculatePercent(options.percent)

        this.launchAnimOnScroll()
    }

    setSizeChart() {
        //Give size for Rétina
        this.$chart.width = this.width * 2
        this.$chart.height = this.heightContainer * 2

        this.$chart.style.width = `${this.width}px`
        this.$chart.style.height = `${this.heightContainer}px`

        this.widthCanvas = this.$chart.width / 2
        this.heightCanvas = this.$chart.height / 2

        this.ctx = this.$chart.getContext("2d")
        this.ctx.scale(2, 2)
    }

    update(percent: number){
        this.start = 0
        this.percentMax = this.calculatePercent(percent)
        this.animation = requestAnimationFrame(this.animChart.bind(this))
    }

    launchAnimOnScroll() {
        if (this.animOnScroll) {
            window.addEventListener("scroll", () => {
                let windowHeight = window.innerHeight

                let posElement = this.$chart.getBoundingClientRect().top
                if (this.$chart.getBoundingClientRect().top < windowHeight - 100 && !this.isAlreadyAnimated) {
                    this.animation = requestAnimationFrame(this.animChart.bind(this))
                    this.isAlreadyAnimated = true
                }
            })
        } else {
            this.animation = requestAnimationFrame(this.animChart.bind(this))
        }
    }

    calculatePercent(percent: number) {
        return percent * (this.widthCanvas - this.paddingCornerRound * 2) / 100
    }

    setLabel(currentpercent: number) {
        this.ctx.font = `${this.fontSize}px Roboto_bold`
        let percentNumber = `${Math.ceil(currentpercent / (this.widthCanvas - this.paddingCornerRound * 2) * 100) - 1}%`
        this.ctx.fillStyle = "white"
        this.ctx.fillText(percentNumber, 10, this.heightCanvas / 2 + 4)
    }

    getColor(currentpercent: number): string {
        if (this.colorsStates) {
            if (currentpercent < 20) {
                return this.colors.bad
            } else if (currentpercent < 60) {
                return this.colors.medium
            } else {
                return this.colors.good
            }
        } else {
            return "#124D72"
        }
    }

    createChart() {
        this.ctx.clearRect(0, 0, this.widthCanvas, this.heightCanvas)

        //Draw line
        this.ctx.beginPath()
        this.ctx.moveTo(this.paddingCornerRound, this.heightCanvas / 2)
        this.ctx.lineTo(this.widthCanvas - this.paddingCornerRound, this.heightCanvas / 2)
        this.ctx.lineWidth = this.heightBar
        this.ctx.strokeStyle = "#D1D3D5"
        this.ctx.lineCap = "round"
        this.ctx.stroke()
        this.ctx.closePath()
    }

    animChart(self: any) {
        this.createChart()

        if (this.start < this.percentMax) {
            this.ctx.beginPath()
            this.ctx.moveTo(this.paddingCornerRound, this.heightCanvas / 2)
            this.ctx.lineTo(this.start++, this.heightCanvas / 2)
            this.ctx.lineWidth = this.heightBar
            this.ctx.strokeStyle = this.getColor(this.start)
            this.ctx.lineCap = "round"
            this.ctx.stroke()
            this.ctx.closePath()

            if (this.displayLegend) {
                this.setLabel(this.start++)
            }

            if (this.start >= this.percentMax) {
                cancelAnimationFrame(this.animation)
            } else {
                window.requestAnimationFrame(this.animChart.bind(this))
            }
        }
    }
}
