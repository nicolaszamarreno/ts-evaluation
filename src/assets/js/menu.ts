declare var require: any
const dynamics: any = require("dynamics.js")

export interface StyleMenu {
    opacity: number
    scale: number
    left?: number
    top?: number
}

export const initializeMenuList = ($button: HTMLElement, $menu: HTMLElement, $mainMenu: boolean = false) => {
    const handleScrollMenuMain = () => {
        window.addEventListener("scroll", event => {
            if ($button.classList.contains("button-menu-main--open")) {
                $button.classList.remove("button-menu-main--open")
                hideMenu()
            }
        })
    }

    const handleClickMenu = () => {
        $button.addEventListener("click", event => {
            const idMenu = $mainMenu ? "menu-main" : "menu_context"

            if ($button.classList.contains(`button-${idMenu}--open`)) {
                $button.classList.remove(`button-${idMenu}--open`)
                hideMenu()
            } else {
                $button.classList.add(`button-${idMenu}--open`)
                showMenu(prepareStyle())
            }
        })
    }

    const getPositionMainMenu = () => {
        const widthButton = $button.getBoundingClientRect().right - $button.getBoundingClientRect().left
        const widthPanel = $menu.offsetWidth

        const posVertical = $button.getBoundingClientRect().right - widthButton / 2 - widthPanel + 35
        const posHorizontal = $button.getBoundingClientRect().bottom + 20

        return [posVertical, posHorizontal]
    }

    const prepareStyle = () => {
        const style: StyleMenu = {
            opacity: 1,
            scale: 1
        }

        if ($mainMenu) {
            const positionMenu = getPositionMainMenu()
            style.left = positionMenu[0]
            style.top = positionMenu[1]
        } else {
            const widthButton = $button.offsetWidth
            const positionPanel = getPosition()
            const widthPanel = $menu.offsetWidth

            const posVertical = positionPanel[0] - widthPanel + (widthButton / 2 + 33)
            const posHorizontal = positionPanel[1] + 45
            style.left = posVertical
            style.top = posHorizontal
        }

        return style
    }

    const getPosition = () => {
        let left = 0
        let top = 0
        let $elem: any = $button

        while ($elem.offsetParent !== undefined && $elem.offsetParent !== null) {
            left += $elem.offsetLeft + ($elem.clientLeft !== null ? $elem.clientLeft : 0)
            top += $elem.offsetTop + ($elem.clientTop !== null ? $elem.clientTop : 0)
            $elem = $elem.offsetParent
        }

        return [left, top]
    }

    const hideMenu = () => {
        dynamics.animate(
            $menu,
            {
                opacity: 0,
                scale: 0.1
            },
            {
                type: dynamics.easeInOut,
                duration: 300,
                friction: 100
            }
        )
    }

    const showMenu = (styleMenu: StyleMenu) => {
        const style = styleMenu
        const $items = $menu.querySelectorAll("li")

        dynamics.animate($menu, style, {
            type: dynamics.spring,
            frequency: 200,
            friction: 600,
            duration: 800
        })

        Array.prototype.forEach.call($items, ($el: Element, index: number) => {
            dynamics.css($el, {
                opacity: 0,
                translateY: 20
            })

            dynamics.animate(
                $el,
                {
                    opacity: 1,
                    translateY: 0
                },
                {
                    type: dynamics.spring,
                    frequency: 300,
                    friction: 435,
                    duration: 1000,
                    delay: 100 + index * 40
                }
            )
        })
    }

    const initializePosition = () => {
        let style = prepareStyle()
        $menu.style.left = style.left + "px"
        $menu.style.top = style.top + "px"
    }

    if ($mainMenu) {
        handleScrollMenuMain()
    }

    handleClickMenu()

    initializePosition()
}
