/**
 * Collapse the list-dropdown
 * To apply the class of the dropdown dropdown-collapse and put id on the list-dropdown
 */
export default class ListDropdown {
    constructor() {
        this.isOpen = false

        let $buttonsDropdown = document.querySelectorAll('.dropdown-collapse')
        $buttonsDropdown.forEach(($el) => {
            /**
             * @event Click
             * Change the arrow direction in the dropdown
             */
            $el.addEventListener('click', (event) => {
                //Get back the id to Element to collapse
                let idList = event.currentTarget.getAttribute('data-collapse')
                document.querySelector(`${idList}`).classList.toggle('list-dropdown--hidden')

                let $iconDirection = document.querySelector('.dropdown__icon')
                if (this.isOpen) {
                    $iconDirection.classList.remove('icon-top')
                    $iconDirection.classList.add('icon-bottom')
                    this.isOpen = false
                }
                else {
                    $iconDirection.classList.remove('icon-bottom')
                    $iconDirection.classList.add('icon-top')
                    this.isOpen = true
                }
            })

        })
    }
}