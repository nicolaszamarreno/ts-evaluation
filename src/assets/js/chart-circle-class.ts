import { Colors } from "../../interfaces/atoms/chart-bar"

export interface OptionsChart {
    size: number
    strokeSize?: number
    animOnScroll?: boolean
    legendDom?: any
    startDegrees?: number
    colorsStates?: boolean
}

class ChartCircleClass {
    private $chart: any
    private ctx: any
    private animation: any
    private legend: any
    private color: Colors
    private maxDegrees: number
    private startDegrees: number
    private start: number
    private size: number
    private sizeStroke: number
    private currentColor: string
    private animOnScroll: boolean
    private colorsStates: boolean
    private isAlreadyAnimated: boolean

    constructor($element: HTMLCanvasElement, options: OptionsChart) {
        this.$chart = $element

        this.maxDegrees = this.getDegrees(this.$chart.getAttribute("data-percent")) - 90
        this.startDegrees = options.startDegrees || -90
        this.start = 0

        this.size = options.size
        this.sizeStroke = options.strokeSize || 10
        this.currentColor = ""
        this.legend = options.legendDom || "canvas"

        this.animOnScroll = options.animOnScroll || false

        this.color = {
            good: "#308649",
            medium: "#FD9141",
            bad: "#D11548"
        }

        this.colorsStates = options.colorsStates || true

        if (this.animOnScroll) {
            this.isAlreadyAnimated = false
        }

        this.setSizeChart()

        this.launchAnimOnScroll()
    }

    setSizeChart() {
        //Give size for Rétina
        this.$chart.width = this.size * 2
        this.$chart.height = this.size * 2
        this.$chart.style.width = `${this.size}px`
        this.$chart.style.height = `${this.size}px`

        this.ctx = this.$chart.getContext("2d")
        this.ctx.scale(2, 2)
    }

    launchAnimOnScroll() {
        if (this.animOnScroll) {
            window.addEventListener("scroll", () => {
                let windowHeight = window.innerHeight

                let posElement = this.$chart.getBoundingClientRect().top
                if (this.$chart.getBoundingClientRect().top < windowHeight - 100 && !this.isAlreadyAnimated) {
                    this.animation = requestAnimationFrame(this.createChart.bind(this))
                    this.isAlreadyAnimated = true
                }
            })
        } else {
            this.animation = requestAnimationFrame(this.createChart.bind(this))
        }
    }

    updateChart(newPourcent: number) {
        this.start = 0
        this.maxDegrees = newPourcent
        this.launchAnimOnScroll()
    }

    createChart(self: any) {
        this.ctx.clearRect(0, 0, this.size * 2, this.size * 2)

        this.ctx.beginPath()
        this.ctx.arc(this.size / 2, this.size / 2, this.size / 2 - this.sizeStroke, 0, Math.PI * 2, true)
        this.ctx.lineWidth = this.sizeStroke
        this.ctx.strokeStyle = "#FAFAFA"
        this.ctx.lineCap = "round"
        this.ctx.stroke()

        this.ctx.beginPath()
        this.ctx.arc(
            this.size / 2,
            this.size / 2,
            this.size / 2 - this.sizeStroke,
            this.degreesToRadians(-90),
            this.degreesToRadians(this.startDegrees++),
            false
        )
        this.ctx.lineWidth = this.sizeStroke
        this.ctx.strokeStyle = this.currentColor
        this.ctx.lineCap = "round"
        this.ctx.stroke()

        this.getLegend()

        this.start++

        if (this.startDegrees === this.maxDegrees) {
            cancelAnimationFrame(this.animation)
        } else {
            window.requestAnimationFrame(this.createChart.bind(this))
        }
    }

    getLegend() {
        if (this.legend != "canvas") {
            let pourcent = Math.ceil(this.start / 360 * 100)
            let color = this.getColor(pourcent)
            this.legend.innerHTML = `${pourcent}%`
            this.currentColor = this.getColor(pourcent)
            this.legend.style.color = this.currentColor
        } else {
            this.ctx.font = "32px Roboto_bold"

            let pourcent = Math.ceil(this.start / 360 * 100)

            this.currentColor = this.getColor(pourcent)

            this.ctx.fillStyle = this.currentColor

            let posXPourcent = this.size / 2 - this.ctx.measureText(`${pourcent}%`).width / 2
            let posYPourcent = this.size / 2 + 12

            this.ctx.fillText(`${pourcent}%`, posXPourcent, posYPourcent)
        }
    }

    degreesToRadians(degrees: number) {
        return degrees * Math.PI / 180
    }

    getDegrees(degrees: number) {
        return 360 * degrees / 100
    }

    getColor(currentPourcent: number) {
        if (this.colorsStates) {
            if (currentPourcent < 20) {
                return this.color.bad
            } else if (currentPourcent < 60) {
                return this.color.medium
            } else {
                return this.color.good
            }
        } else {
            return "#124D72"
        }
    }
}

export { ChartCircleClass }
