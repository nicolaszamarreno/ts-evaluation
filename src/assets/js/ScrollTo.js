const ScrollTo = (element, to, duration) => {
    if (duration <= 0) return
    let difference = to - element.scrollTop
    let perTick = difference / duration * 10

    setTimeout(() => {
        element.scrollTop = element.scrollTop + perTick
        if (element.scrollTop === to) return
        ScrollTo(element, to, duration - 10)
    }, 10)
}

export { ScrollTo }

