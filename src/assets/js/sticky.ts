export interface OptionsSticky {
    className: string
    elementAddClass: HTMLElement
}

class Sticky {
    private elementTarget: HTMLElement
    private className: string
    private elementAddClass: HTMLElement
    private stuck: boolean
    private stickPoint: number

    constructor(elementIsLooked: HTMLElement, options: OptionsSticky) {
        this.elementTarget = elementIsLooked
        this.className = options.className
        this.elementAddClass = options.elementAddClass

        this.stuck = false
        this.stickPoint = this.getDistance(this.elementTarget)

        this.handleScroll()
    }

    /**
     * Return distance
     * @param $element {Node}
     * @returns {number}
     */
    getDistance($element: HTMLElement) {
        return $element.offsetTop
    }

    /**
     * Init Scroll Event
     */
    handleScroll() {
        window.addEventListener("scroll", event => {
            let distance = this.getDistance(this.elementTarget) - window.pageYOffset
            let offset = window.pageYOffset
            let $el = this.elementAddClass ? this.elementAddClass : this.elementTarget

            if (distance <= 0 && !this.stuck) {
                $el.classList.add(this.className)
                this.stuck = true
            } else if (this.stuck && offset <= this.stickPoint) {
                $el.classList.remove(this.className)
                this.stuck = false
            }
        })
    }
}

export { Sticky }
