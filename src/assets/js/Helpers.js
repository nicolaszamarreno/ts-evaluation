export default class Helpers {
  animationIconStats($elements, time) {
    if (document.querySelectorAll($elements).length > 0) {
      document.querySelectorAll($elements).forEach($item => {
        let pourcent = $item.getAttribute("data-pourcent");

        $item.style.transition = `width ${time}s`;
        $item.style.width = `${pourcent}%`;
      });
    }
  }
}
