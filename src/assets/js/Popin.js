class Popin {
    constructor() {
        this.buttonClass = '.popin__close'
        this.addEvents()
    }

    getPopin() {
        this.popins = document.querySelector('.popin')
    }

    addEvents() {
        //Add display on Overlaid
        let $overlaid = document.querySelector('.popin__overlaid')
        if ($overlaid) {
            $overlaid.addEventListener('click', () => {
                this.animPopin()
            })

            //Add display on buttons
            document.querySelectorAll(this.buttonClass).forEach(($button) => {
                $button.addEventListener('click', () => {
                    this.animPopin()
                })
            })
        }

    }

    animPopin() {
        let $popin = document.querySelector('.popin__wrapper');
        $popin.classList.remove('popin__wrapper--enter');
        $popin.classList.add('popin__wrapper--leave');

        setTimeout(() => {
            let $overlaid = document.querySelector('.popin__overlaid');
            $overlaid.classList.add('popin__overlaid--novisible');
        }, 500)
    }
}

export default Popin