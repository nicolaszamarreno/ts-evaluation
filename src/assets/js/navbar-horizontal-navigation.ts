export interface Event {
    currentTarget: HTMLElement
}

class NavbarHorizontalClass {
    private $status: HTMLElement
    private $itemsNavbarHorizontal: NodeList

    constructor() {
        this.$status = document.querySelector(".navbar-status--horizontal") as HTMLElement
        this.$itemsNavbarHorizontal = document.querySelectorAll(".navbar--horizontal li") as NodeListOf<HTMLElement>
        let ItemMenu = this.$itemsNavbarHorizontal[0] as HTMLElement
        let widthItem = ItemMenu.offsetWidth

        this.$status.style.transition = "all .5s"

        this.initEvent()
    }

    initEvent() {
        const nodelistToArray = Array.prototype.slice.call(this.$itemsNavbarHorizontal)
        for (let $el of nodelistToArray) {
            $el.addEventListener("mouseenter", (event: Event) => {
                let position = event.currentTarget.offsetLeft
                this.$status.style.transform = `translateX(${position}px)`
                this.$status.style.width = `${event.currentTarget.offsetWidth}px`
            })

            $el.addEventListener("mouseleave", (event: Event) => {
                let itemIsSelected = document.querySelector(".navbar--horizontal--active")!.parentNode as HTMLElement
                let position = itemIsSelected.offsetLeft
                this.$status.style.transform = `translateX(${position}px)`
                this.$status.style.width = `${itemIsSelected.offsetWidth}px`
            })
        }
    }
}

export { NavbarHorizontalClass }
