export default class Comments {

    /**
     *
     * @param $elementTarget {Node}
     */
    constructor($elementTarget) {
        this.wrapperComment = $elementTarget
        this.handleScroll()
    }

    /**
     * Init Scroll Event
     */
    handleScroll() {
        this.wrapperComment.addEventListener('scroll', (event) => {
            let positionScroll = document.querySelector('.timeline__overflow').scrollTop
            let $el = document.querySelectorAll('.timeline--comment .timeline__item')

            $el.forEach(($item) => {
                if (positionScroll > $item.offsetTop) {
                    let date = $item.querySelector('.feedback').getAttribute('data-date').split(' ')

                    document.querySelector('.timeline--comment .timeline__date strong').innerHTML = date[0]
                    document.querySelector('.timeline--comment .timeline__date span').innerHTML = date[1]
                }
            })
        })
    }

}


