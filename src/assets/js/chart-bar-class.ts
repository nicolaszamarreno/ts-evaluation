import {CoordinateBar, DatasReceive, Colors, OptionsChart} from "../../interfaces/atoms/chart-bar"

export interface DatasChart {
    percent: number
    date: string
    maxPercentInPixel: number
    currentPercent: number
}

export interface BestIndicator {
    value: number
    index: number
}

export interface RadiusBar {
    tl: number
    tr: number
    br: number
    bl: number
}

class ChartBarClass {
    private $chart: any
    private ctx: any
    private animation: any
    private colors: Colors
    private bestIndicator: BestIndicator
    private animOnScroll: boolean
    private colorsStates: boolean
    private isAlreadyAnimated: boolean
    private height: number
    private width: number
    private speedAnimation: number
    private margin: number
    private widthCanvas: number
    private heightCanvas: number
    private datas: DatasChart[]
    private barElements: CoordinateBar[]
    private registerElement: boolean

    constructor($container: HTMLCanvasElement, options: OptionsChart) {
        this.$chart = $container

        this.height = options.height || 200
        this.width = options.width || this.$chart.parentElement.offsetWidth
        this.speedAnimation = options.speedAnimation || 1
        this.margin = 15

        this.colors = options.colors || {
            good: "#308649",
            medium: "#FD9141",
            bad: "#D11548"
        }

        this.barElements = []

        this.animOnScroll = options.animOnScroll || true

        this.colorsStates = options.colorsStates || false

        this.setSizeChart()

        if (this.animOnScroll) {
            this.isAlreadyAnimated = false
        }

        this.init(options.datas)
    }

    init(datas: DatasReceive[]) {
        this.datas = this.calculatePercent(datas)

        this.registerElement = true

        if (!this.colorsStates) {
            this.getHightIndicator()
        }

        this.launchAnimOnScroll()
    }

    update(datas: DatasReceive[]) {
        this.datas = this.calculatePercent(datas)

        this.barElements = []
        this.registerElement = true

        if (!this.colorsStates) {
            this.getHightIndicator()
        }

        this.animation = requestAnimationFrame(this.createChart.bind(this))
    }

    getHightIndicator() {
        this.bestIndicator = {
            index: 0,
            value: 0
        }

        this.datas.filter((element, index) => {
            if (element.percent > this.bestIndicator.value) {
                this.bestIndicator = {
                    index: index,
                    value: element.percent
                }
            }
        })
    }

    setSizeChart() {
        // Give size for Rétina
        this.$chart.width = this.width * 2
        this.$chart.height = this.height * 2

        this.$chart.style.width = `${this.width}px`
        this.$chart.style.height = `${this.height}px`

        this.widthCanvas = this.$chart.width / 2
        this.heightCanvas = this.$chart.height / 2

        this.ctx = this.$chart.getContext("2d")
        this.ctx.scale(2, 2)
    }

    launchAnimOnScroll() {
        window.addEventListener("scroll", () => {
            let windowHeight = window.innerHeight

            let posElement = this.$chart.getBoundingClientRect().top
            if (this.$chart.getBoundingClientRect().top < windowHeight - 100 && !this.isAlreadyAnimated) {
                this.animation = requestAnimationFrame(this.createChart.bind(this))
                this.isAlreadyAnimated = true
            }
        })
    }

    calculatePercent(arrayDatas: any) {
        for (let i = 0; i < arrayDatas.length; i++) {
            arrayDatas[i]["maxPercentInPixel"] = (this.heightCanvas - (this.margin * 2)) * arrayDatas[i].percent / 100
            arrayDatas[i]["currentPercent"] = 0
        }

        return arrayDatas
    }

    getPercent(percent: number) {
        return Math.ceil(percent / (this.heightCanvas - (this.margin * 2)) * 100) - 1
    }

    getLegend(positionX: number, positionY: number, widthBar: number, heightBar: number, percent: number, legend: string) {
        this.ctx.font = "12px Roboto_light"

        let percentNumber = `${this.getPercent(percent)}%`

        let posXPercent = (widthBar - this.ctx.measureText(percentNumber).width) / 2 + positionX
        let posYPercent = positionY - 5

        let posXLegend = (widthBar - this.ctx.measureText(legend).width) / 2 + positionX
        let posYLegend = this.heightCanvas

        this.ctx.fillText(percentNumber, posXPercent, posYPercent)
        this.ctx.fillText(legend, posXLegend, posYLegend)
    }

    getColor(currentPercent: number, index: number): string {
        if (this.colorsStates) {
            if (currentPercent < 20) {
                return this.colors.bad
            } else if (currentPercent < 60) {
                return this.colors.medium
            } else {
                return this.colors.good
            }
        } else {
            return this.bestIndicator.index === index ? "#5A9CDE" : "#124D72"
        }
    }

    createChart(self: any) {
        this.ctx.clearRect(0, 0, this.widthCanvas, this.heightCanvas)

        this.createLineLegend()

        // Initialize width bar
        let widthBar = (this.widthCanvas - this.margin * 2) / this.datas.length - this.margin

        // Set width minimun
        widthBar = widthBar > 50 ? 50 : widthBar

        let chartIsRender = 0

        for (let i = 0; i < this.datas.length; i++) {
            // Set position and Height Chart
            let posX = widthBar * i + this.margin * (1 + i)
            let posY = this.heightCanvas - (this.datas[i]["currentPercent"] + this.margin)
            let widthChartBar = widthBar
            let heightChartBar = this.datas[i]["currentPercent"]

            if (this.registerElement) {
                this.barElements.push({
                    startX: posX,
                    endX: posX + widthChartBar,
                    startY: this.heightCanvas - this.margin,
                    endY: this.heightCanvas - this.margin - this.datas[i]["maxPercentInPixel"],
                    width: widthChartBar,
                    height: this.datas[i]["maxPercentInPixel"]
                })
            }

            if (this.datas[i]["currentPercent"] <= this.datas[i]["maxPercentInPixel"]) {
                this.datas[i]["currentPercent"] = this.datas[i]["currentPercent"] + this.speedAnimation

                this.drawBarRounded(
                    posX,
                    posY,
                    widthChartBar,
                    heightChartBar,
                    { tl: 15, tr: 15, br: 0, bl: 0 },
                    this.getColor(this.getPercent(this.datas[i]["currentPercent"]), i)
                )

                this.getLegend(posX, posY, widthChartBar, heightChartBar, this.datas[i]["currentPercent"], this.datas[i]["date"])
            } else {
                this.drawBarRounded(
                    posX,
                    posY,
                    widthChartBar,
                    heightChartBar,
                    { tl: 15, tr: 15, br: 0, bl: 0 },
                    this.getColor(this.getPercent(this.datas[i]["currentPercent"]), i)
                )

                this.getLegend(posX, posY, widthChartBar, heightChartBar, this.datas[i]["currentPercent"], this.datas[i]["date"])

                chartIsRender++
            }
        }

        this.registerElement = false

        if (chartIsRender === this.datas.length) {
            cancelAnimationFrame(this.animation)
        } else {
            window.requestAnimationFrame(this.createChart.bind(this))
        }
    }

    setElements() {
        return this.barElements
    }

    createLineLegend() {
        this.ctx.beginPath()
        this.ctx.moveTo(this.margin, this.heightCanvas - this.margin)
        this.ctx.lineTo(this.widthCanvas - this.margin, this.heightCanvas - this.margin)
        this.ctx.strokeStyle = "black"
        this.ctx.lineWidth = 0.5
        this.ctx.stroke()
    }

    /**
     * Draws a rounded rectangle using the current state of the canvas.
     * @param {CanvasRenderingContext2D} ctx
     * @param {Number} x The top left x coordinate
     * @param {Number} y The top left y coordinate
     * @param {Number} width The width of the rectangle
     * @param {Number} height The height of the rectangle
     * @param {Number} [radius.tl = 0] Top left
     * @param {Number} [radius.tr = 0] Top right
     * @param {Number} [radius.br = 0] Bottom right
     * @param {Number} [radius.bl = 0] Bottom left
     */
    drawBarRounded(x: number, y: number, width: number, height: number, radius: RadiusBar, color: string) {
        this.ctx.beginPath()
        this.ctx.moveTo(x + radius.tl, y)
        this.ctx.lineTo(x + width - radius.tr, y)
        this.ctx.quadraticCurveTo(x + width, y, x + width, y + radius.tr)
        this.ctx.lineTo(x + width, y + height - radius.br)
        this.ctx.quadraticCurveTo(x + width, y + height, x + width - radius.br, y + height)
        this.ctx.lineTo(x + radius.bl, y + height)
        this.ctx.quadraticCurveTo(x, y + height, x, y + height - radius.bl)
        this.ctx.lineTo(x, y + radius.tl)
        this.ctx.quadraticCurveTo(x, y, x + radius.tl, y)
        this.ctx.fillStyle = color

        this.ctx.fill()
        this.ctx.closePath()
    }
}

export { ChartBarClass }
