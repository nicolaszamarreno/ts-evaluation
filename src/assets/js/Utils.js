const Utils = {
    findParent: (element, parentName) => {
        let elementSearchParent = element
        let elementIsFind = false;

        while (elementIsFind === false) {
            elementSearchParent = elementSearchParent.parentNode

            if (elementSearchParent.classList.contains(parentName)) {
                elementIsFind = true
            }
        }

        return elementSearchParent
    },
    debounce: (callback, delay) => {
        var timer;
        return () => {
            var args = arguments;
            var context = this;
            clearTimeout(timer);
            timer = setTimeout(() => {
                callback.apply(context, args);
            }, delay)
        }
    }
}

export default Utils