export default class StatsBar {
    /**
     * @param element {Node}
     */
    constructor($element) {
        this.elStatsBar = $element

        this.animateBar()
    }

    animateBar() {
        let max = this.elStatsBar.getAttribute('data-max')
        let current = this.elStatsBar.getAttribute('data-current')
        this.elStatsBar.querySelector('.stats-bar__result').style.transform = `translate(${this.calculatePourcent(current, max)}%, -50%)`
    }

    calculatePourcent(current, max) {
        return (current / max) * 100
    }
}