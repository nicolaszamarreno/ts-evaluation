import StatsBar from './Stats-bar'

export default class ScrollAction {
    constructor() {
        this.$elements = document.querySelectorAll('.stats-bar')
        this.offsetLaunchAnim = 100
        this.applyEffect()
    }

    applyEffect() {
        window.addEventListener('scroll', () => {
            let windowHeight = window.innerHeight
            this.$elements.forEach(($item) => {
                let posElement = $item.getBoundingClientRect().top
                if ($item.getBoundingClientRect().top < (windowHeight - this.offsetLaunchAnim)) {
                    new StatsBar($item)
                }
            })
        })

    }
}