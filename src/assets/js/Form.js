class Form {
    constructor() {
        this.dropdown()
    }

    dropdown() {
        let $dropdownList = document.querySelectorAll('.input-select__button')
        let $dropdownChoices = document.querySelectorAll('.input-select__item')

        const toggleDropdown = (el) => {
            el.currentTarget.parentNode.classList.toggle('input-select--open')
        }

        const selectChoice = (el) => {
            let content = el.currentTarget.innerHTML
            let $parent = GetParent(el.currentTarget, 'input-select')

            $parent.querySelector('.input-select__label').innerHTML = content
            document.querySelector('.input-select--open').classList.remove('input-select--open')

        }

        if ($dropdownList && $dropdownChoices) {
            $dropdownList.forEach(($item) => {
                $item.addEventListener('click', toggleDropdown)
            })

            $dropdownChoices.forEach(($item) => {
                $item.addEventListener('click', selectChoice)
            })
        }

    }
}

export default Form