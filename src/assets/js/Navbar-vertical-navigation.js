import dynamics from 'dynamics.js'
import Utils from './Utils'
import { ScrollTo } from './ScrollTo'

export default class NavbarVertical {
    /**
     * Offset top detect top of Element
     * @param offsetSpy {Number}
     */
    constructor(offsetSpy) {
        this.$status = document.querySelector('.navbar-status--vertical')
        this.$statusShowActive = document.querySelector('.navbar-status--showActive')
        this.$itemsNavbarVertical = document.querySelectorAll('.navbar--vertical li')
        this.heightItem = this.$itemsNavbarVertical[0].offsetHeight

        /**
         * Catch the attribute href for ScrollTo
         * @type {NodeList}
         */
        this.$navigationItem = document.querySelectorAll('.navbar--vertical li a')

        this.offsetToSpy = offsetSpy
        this.inScrolling = false

        this.$status.style.height = `${this.heightItem}px`
        this.$statusShowActive.style.height = `${this.heightItem}px`

        this.initEvents()
        this.initScrollTo()
        this.initScrollSpy()
    }

    /**
     * Init all Events on the Menu Vertical
     */
    initEvents() {
        /**
         * Parse menu elements and attribute Events
         */
        this.$itemsNavbarVertical.forEach(($el) => {
            /**
             * @event mouseenter
             */
            $el.addEventListener('mouseenter', (event) => {
                this.$status.style.transition = 'all .5s'
                let index = Array.prototype.indexOf.call($el.parentNode.children, $el)
                this.$status.style.transform = `translateY(${index * this.heightItem}px)`
            })

            /**
             * @event mouseleave
             */
            $el.addEventListener('mouseleave', (event) => {
                this.$status.style.transition = 'all .5s'
                let itemIsSelected = document.querySelector('.navbar--vertical--active')
                let index = Array.prototype.indexOf.call(itemIsSelected.parentNode.children, itemIsSelected)
                this.$status.style.transform = `translateY(${index * this.heightItem}px)`
            })

            /**
             * @event click
             */
            $el.addEventListener('click', (event) => {
                this.animationMenu(event.currentTarget)
            })
        })
    }


    /**
     * Create animation when i select the item menu
     * @param eventTarget {Node}
     */
    animationMenu(eventTarget) {
        this.$status.style.transition = 'all 0s'
        document.querySelector('.navbar--vertical--active').classList.remove('navbar--vertical--active')
        eventTarget.classList.add('navbar--vertical--active')
        dynamics.animate([this.$status, this.$statusShowActive], {
            translateY: eventTarget.offsetTop
        }, {
                type: dynamics.spring,
                friction: 954,
                anticipationSize: 371,
                anticipationStrength: 67
            })
    }

    /**
     * Allow to change item selected when i pass a section panel
     */
    initScrollSpy() {
        let $sections = document.querySelectorAll('.panel')

        const animMenu = ($element) => {
            let idToGo = $element.getAttribute('id')
            let $el = document.querySelector(`a[href="#${idToGo}"]`)
            this.animationMenu($el.parentNode)
        }

        /**
         * @event scroll
         */
        window.addEventListener('scroll', Utils.debounce((e) => {
            $sections.forEach(($item, index) => {
                if ($item.getBoundingClientRect().top < $item.offsetHeight
                    && $item.getBoundingClientRect().top < this.offsetToSpy + 5
                ) {
                    animMenu($item)
                }
                //
                if (index === 0 && $item.getBoundingClientRect().top > this.offsetToSpy + 5) {
                    animMenu($item)
                }
            })
        }, 250))
    }

    /**
     * Init ScrollTo
     */
    initScrollTo() {
        this.$navigationItem.forEach(($el) => {
            /**
             * @event click
             */
            $el.addEventListener('click', (event) => {
                event.preventDefault()

                this.inScrolling = true
                let scrollPosition = document.documentElement.scrollTop || document.body.scrollTop
                let idSection = event.currentTarget.getAttribute('href')
                let posTop = document.querySelector(`${idSection}`).getBoundingClientRect().top
                let scrollTo = (scrollPosition + posTop) - this.offsetToSpy + 5
                ScrollTo((document.documentElement || document.body.parentNode || document.body), scrollTo, 600)
                setTimeout(() => {
                    this.inScrolling = false
                }, 600)

            })
        })
    }
}