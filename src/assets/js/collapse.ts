export interface OptionsCollapse {
    buttonCollapse: HTMLElement
    contentWrapper: string
    addClassButtonCollapse?: string
    time: string
    idWrapper: string
}

class Collapse {
    private $contentWrapper: HTMLElement
    private $buttonCollapse: HTMLElement
    private $idWrapper: HTMLElement
    private time: string
    private heightContent: string
    private wrapperIsOpen: boolean

    constructor(options: OptionsCollapse) {
        this.$contentWrapper = document.querySelector(options.contentWrapper) as HTMLElement
        this.$buttonCollapse = options.buttonCollapse
        this.time = options.time
        this.$idWrapper = document.querySelector(options.idWrapper) as HTMLElement
        this.wrapperIsOpen = false

        this.$idWrapper.addEventListener("transitionend", event => {
            if (this.wrapperIsOpen) {
                this.$idWrapper.style.maxHeight = "auto"
            }
        })
    }

    collapse() {
        const originHeight = this.$contentWrapper.offsetHeight
        this.$idWrapper.style.transition = `max-height ${this.time}s`
        let height = this.$idWrapper.ownerDocument.defaultView.getComputedStyle(this.$idWrapper, "").height as string

        if (parseInt(height, 10) === 0) {
            this.$idWrapper.style.maxHeight = `${originHeight}px`
            this.wrapperIsOpen = !this.wrapperIsOpen
        } else {
            this.$idWrapper.style.maxHeight = `${originHeight}px`
            this.$idWrapper.style.maxHeight = "0px"
            this.wrapperIsOpen = !this.wrapperIsOpen
        }
    }
}

export { Collapse }
