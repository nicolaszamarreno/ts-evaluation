import { ObjectivesProps } from "./views/objectives/objectives"
import { observable, computed, action } from "mobx"

export interface Objective {
    date: string
    percent: number
    describe: string
    author: string
}

export interface ObjectiveHistory {
    id: number
    title: string
    tag: string
    describe: string
    created: string
    author: string
    status: string
    picture: string
    progress: Objective[]
}

export interface User {
    id: number
    name: string
    username: string
    email: string
    address: object
    phone: string
    website: string
    company: object
}

export class ObjectivesStore {
    @observable notes = [
        {
            title: "Heolo",
            icon: "icon-notification",
            score: 23
        },
        {
            title: "Heolo",
            icon: "icon-message2",
            score: 15
        },
        {
            title: "Heolo",
            icon: "icon-phone",
            score: 10
        }
    ]

    @observable individual: User

    @observable objectives = [
        {
            id: 1,
            title: "Communicate team news to other departments of the compagny",
            tag: "Personal",
            describe:
                "Tempore quo primis auspiciis in mundanum fulgorem surgeret victura dum erunt homines Roma, ut augeretur sublimibus incrementis,Tempore quo primis auspicii… Tempore quo primis auspiciis in mundanum fulgorem surgeret victura dum erunt homines Roma, ut augeretur sublimibus incrementis,Tempore quo primis auspicii… Tempore quo primis auspiciis in mundanum fulgorem surgeret victura dum erunt homines Roma, ut augeretur sublimibus incrementis,Tempore quo primis auspicii…",
            created: "24/07/17",
            author: "Jean-Paul Aubin",
            status: "connect",
            picture:
                "https://i.pinimg.com/736x/f5/a0/62/f5a0626a80fe6026c0ac65cdc2d8ede2--photography-portraits-photography-people.jpg",
            progress: [
                {
                    date: "23/23",
                    percent: 50,
                    describe: "Tempore quo primis auspiciis in mundanum fulgorem surgeret victura dum erunt homines Roma, ut augeretur sublimibus incrementis,Tempore quo primis auspicii… Tempore quo primis auspiciis in mundanum fulgorem surgeret victura dum erunt homines Roma, ut augeretur sublimibus incrementis,Tempore quo primis auspicii… Tempore quo primis auspiciis in mundanum fulgorem surgeret victura dum erunt homines Roma, ut augeretur sublimibus incrementis,Tempore quo primis auspicii…",
                    author: "Jean-Paul Albin"
                },
                {
                    date: "12/04",
                    percent: 100,
                    describe: "Colonne 2",
                    author: "Jean-Paul Albin"
                }
            ]
        },
        {
            id: 2,
            title: "Communicate team news to other departments of the compagny",
            tag: "Personal",
            describe:
                "Tempore quo primis auspiciis in mundanum fulgorem surgeret victura dum erunt homines Roma, ut augeretur sublimibus incrementis,Tempore quo primis auspicii…",
            percent: 60,
            created: "16/07/17",
            author: "Alexis Hello",
            status: "connect",
            picture:
                "https://i.pinimg.com/736x/f5/a0/62/f5a0626a80fe6026c0ac65cdc2d8ede2--photography-portraits-photography-people.jpg",
            progress: [
                {
                    date: "23/23",
                    percent: 34,
                    describe:
                        "Tempore quo primis auspiciis in mundanum fulgorem surgeret victura dum erunt homines Roma, ut augeretur sublimibus incrementis,Tempore quo primis auspicii…",
                    author: "Jean-Paul Albin"
                }
            ]
        },
        {
            id: 3,
            title: "Communicate team news to other departments of the compagny",
            tag: "Personal",
            describe:
                "Tempore quo primis auspiciis in mundanum fulgorem surgeret victura dum erunt homines Roma, ut augeretur sublimibus incrementis,Tempore quo primis auspicii…",
            percent: 40,
            created: "10/06/17",
            author: "Jean-Paul Aubin",
            status: "connect",
            picture:
                "https://i.pinimg.com/736x/f5/a0/62/f5a0626a80fe6026c0ac65cdc2d8ede2--photography-portraits-photography-people.jpg",
            progress: [
                {
                    date: "23/23",
                    percent: 34,
                    describe:
                        "Tempore quo primis auspiciis in mundanum fulgorem surgeret victura dum erunt homines Roma, ut augeretur sublimibus incrementis,Tempore quo primis auspicii…",
                    author: "Jean-Paul Albin"
                }
            ]
        }
    ]

    @action addProgress(allProgress: Objective[], progress: Objective) {
        allProgress.push(progress)
    }

    @action fetchApp() {
        let rootURL = "https://jsonplaceholder.typicode.com/users/1"

        return fetch(rootURL,{ method: "GET"})
        .then((response) => {
            return response.json()
        })
        .then((response) => {
            this.individual = response
            return response
        })
        .catch((err) => {
            return null
        })
    }
}
