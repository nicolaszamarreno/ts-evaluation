export interface CoordinateBar {
    startX: number
    startY: number
    endX: number
    endY: number
    width: number
    height: number
}

export interface DatasReceive {
    percent: number
    date: string
}

export interface Colors {
    good: string
    medium: string
    bad: string
}

export interface OptionsChart {
    height: number
    datas: DatasReceive[]
    width?: number
    speedAnimation?: number
    colors?: Colors
    animOnScroll?: boolean
    colorsStates?: boolean
}
