import * as React from "react"
import * as ReactDOM from "react-dom"
import { PanelHeader } from "../../components/molecules/panel-header"
import { BlockObjective } from "../objectives/components/block-objective"
import { Panel } from "../../components/organisms/panel"
import { observer, inject } from "mobx-react"
import { ObjectivesStore } from "../../store"
import { ObjectiveResume } from "./components/objective-resume"

export interface ObjectivesProps {
    datas: ObjectivesStore
}

export interface Notes {
    title: string
    icon: string
    score: number
}

@inject("datas")
@observer
class Objectives extends React.Component<ObjectivesProps, {}> {
    constructor(props: ObjectivesProps) {
        super(props)
    }

    render() {
        return (
            <div className="objectives">
                <div className="objectives-menu background-color--smart">
                    <ul className="objectives-menu__timeline">
                        <li className="objectives-menu__item objectives-menu__item--active"> 2014 </li>
                        <li className="objectives-menu__item"> 2015 </li>
                        <li className="objectives-menu__item"> 2016 </li>
                    </ul>
                </div>
                <div className="layout evaluation">
                    <Panel>
                        <PanelHeader title="Objectives" badgeValue={this.props.datas.objectives.length} />
                        <BlockObjective notes={this.props.datas.notes} />
                    </Panel>
                    {this.renderObjectives()}
                </div>
            </div>
        )
    }

    renderObjectives() {
        return this.props.datas.objectives.map((item, index) => {
            return <ObjectiveResume key={index} objectiveHistory={item} addProgress={this.props.datas.addProgress} />
        })
    }
}

export { Objectives }
