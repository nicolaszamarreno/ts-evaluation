import * as React from "react"
import { ListNotes } from "../../../components/organisms/list-notes"
import { ChartCircle } from "../../../components/atoms/chart-circle"

export interface Notes {
    title: string
    icon: string
    score: number
}

export interface BlockObjectiveProps {
    title?: string
    notes: Notes[]
}

class BlockObjective extends React.Component<BlockObjectiveProps, {}> {
    constructor(props: BlockObjectiveProps) {
        super(props)
    }

    render() {
        return (
            <div className="container no-padding">
                <div className="row no-margin">
                    <div className="layout-2-3 background--adept">
                        <ListNotes notes={this.props.notes} />
                    </div>
                    <div className="layout-1-3 d-flex flex-column justify-content-center align-items-center">
                        <div className=" chart-circle">
                            <ChartCircle size={140} percent={60} strokeSize={12} />
                        </div>
                        <p className="text--regular color--namek">Objectives completion rate</p>
                    </div>
                </div>
            </div>
        )
    }
}

export { BlockObjective }
