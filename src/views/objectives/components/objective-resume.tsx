import * as React from "react"
import { observer } from "mobx-react"
import { Objective, ObjectiveHistory } from "../../../store"
import ReactCSSTransitionGroup = require("react-addons-css-transition-group")
import { PopinObjective } from "./popin-objective"
import { ButtonPrimary } from "../../../components/atoms/button-primary"
import { ChartStat } from "../../../components/atoms/chart-stat"
import { Container } from "../../../components/templates/container"
import { Panel } from "../../../components/organisms/panel"
import { Row } from "../../../components/templates/row"
import { Tag } from "../../../components/atoms/tag"
import { Feed } from "../../../components/molecules/feed"
import { WrapperCollapse } from "../../../components/organisms/wrapper-collapse"
import { Layout } from "../../../components/templates/layout"
import { WrapperControl } from "../../../components/molecules/wrapper-control"
import { ChartBar } from "../../../components/atoms/chart-bar"

interface ObjectiveResumeProps {
    objectiveHistory: ObjectiveHistory
    addProgress: (currentObjectives: Objective[], objectiveToBeAdded: Objective) => void
}

interface ObjectiveResumeState {
    showPopin: boolean
    reRenderChart: boolean
    currentBarChartSelected: number
}

@observer
class ObjectiveResume extends React.Component<ObjectiveResumeProps, ObjectiveResumeState> {
    constructor(props: ObjectiveResumeProps) {
        super(props)

        this.state = {
            showPopin: false,
            reRenderChart: true,
            currentBarChartSelected: 0
        }
    }

    getDatasChart(index?: number) {
        if (typeof index === "number") {
            this.setState({
                reRenderChart: false,
                currentBarChartSelected: index
            })
        }
    }

    displayPopin() {
        this.setState({
            showPopin: !this.state.showPopin,
            reRenderChart: false
        })
    }

    getAverageAccomplished() {
        let totalProgress = this.props.objectiveHistory.progress.length
        let calculProgress = 0

        for (let i = 0; i < totalProgress; i++) {
            calculProgress = calculProgress + this.props.objectiveHistory.progress[i].percent
        }

        return calculProgress / totalProgress
    }

    handleSubmit(percent: number, note: string) {
        let objective = {
            date: "23/23",
            percent: percent,
            describe: note,
            author: "Jean-Paul Albin"
        }

        this.props.addProgress(this.props.objectiveHistory.progress, objective)

        this.setState({
            showPopin: !this.state.showPopin,
            reRenderChart: true,
            currentBarChartSelected: this.props.objectiveHistory.progress.length - 1
        })
    }

    render() {
        const { id, title, tag, describe, created, author, status, picture, progress } = this.props.objectiveHistory

        return (
            <Panel>
                {this.state.showPopin && (
                    <PopinObjective closePopin={this.displayPopin.bind(this)} submitForm={this.handleSubmit.bind(this)} />
                )}

                <Container classModifier="pt-20 pb-20 border border--bottom-none border-radius--top">
                    <Row classModifier="pt-20 pb-20">
                        <div className="layout-2-3">
                            <div className="block-objective__title">
                                <strong className="text--bold">{title}</strong>
                                <Tag colorBackground="background-color--apprentice" modifierClass="block-objective__tag">
                                    {tag}
                                </Tag>
                            </div>

                            <Feed classComponent="mb-20" typeAvatar="feed" status="connect" picture={picture}>
                                Created on {created} <br />
                                by <strong>{author}</strong>
                            </Feed>

                            <span className="label">Description</span>
                            <p>{describe}</p>
                        </div>
                        <div className="layout-1-3">
                            <div className="block-objective__visualize">
                                <ul className="list-elements">
                                    <li className="list-elements__item">
                                        <ChartStat
                                            percent={this.getAverageAccomplished()}
                                            colorsStates={true}
                                            reRender={this.state.reRenderChart}
                                        />
                                    </li>

                                    <li className="list-elements__item">
                                        <ButtonPrimary
                                            size="normal"
                                            status="default"
                                            classComponent="w-100"
                                            onClick={() => this.displayPopin()}
                                        >
                                            Add Evaluation
                                        </ButtonPrimary>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </Row>
                </Container>

                <WrapperCollapse id={`objective-${this.props.objectiveHistory.id}`}>
                    <Container classModifier="pt-20 pb-20 border border--bottom-none border--top-none">
                        <Row classModifier="pt-20 pb-20">
                            <ChartBar
                                datas={this.props.objectiveHistory.progress}
                                heightChart={200}
                                getIndexBar={this.getDatasChart.bind(this)}
                                reRender={this.state.reRenderChart}
                            />
                        </Row>
                        <Row>
                            <Layout classModifier="layout-2-3">
                                <ReactCSSTransitionGroup
                                    transitionName="block-objective__animation"
                                    transitionEnterTimeout={500}
                                    transitionLeaveTimeout={300}
                                >
                                    <div key={this.state.currentBarChartSelected}>
                                        <p>
                                            <strong className="color--terran">Evaluation</strong> created on by Jean-Paul Albin{" "}
                                        </p>
                                        <span className="label">Description</span>
                                        <p>{this.props.objectiveHistory.progress[this.state.currentBarChartSelected].describe}</p>
                                    </div>
                                </ReactCSSTransitionGroup>
                            </Layout>
                        </Row>
                    </Container>
                </WrapperCollapse>
                <WrapperControl idWrapper={`#objective-${this.props.objectiveHistory.id}`}>
                    <div className="block-objective__dropdown">
                        <i className="icon-bottom color--smart" />
                    </div>
                </WrapperControl>
            </Panel>
        )
    }
}

export { ObjectiveResume }
