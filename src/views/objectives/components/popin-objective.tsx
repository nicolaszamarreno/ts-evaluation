import * as React from "react"
import { ButtonSecondary } from "../../../components/atoms/button-secondary"
import { ButtonPrimary } from "../../../components/atoms/button-primary"
import { InputLabel } from "../../../components/molecules/input-label"
import { TextareaLabel } from "../../../components/molecules/textarea-label"
import { Popin } from "../../../components/organisms/popin"
import { FormGroup } from "../../../components/organisms/form-group"

interface PopinObjectiveProps {
    closePopin: () => void
    submitForm: (percent: number, note: string) => void
}

interface PopinObjectiveState {
    percentObjective: number
    noteObjective: string
    fieldValid: boolean
}

class PopinObjective extends React.Component<PopinObjectiveProps, PopinObjectiveState> {
    constructor(props: PopinObjectiveProps) {
        super(props)

        this.state = {
            percentObjective: 0,
            noteObjective: "",
            fieldValid: true
        }
    }

    valuePercentObjective(event: React.FormEvent<HTMLInputElement>) {
        let value: number = parseInt(event.currentTarget.value)

        if (!isNaN(value) && value > 0 && value <= 100) {
            this.setState({
                percentObjective: value,
                fieldValid: true
            })
        } else {
            this.setState({
                fieldValid: false
            })
        }
    }

    valueNoteObjective(event: React.FormEvent<HTMLInputElement>) {
        let note: string = event.currentTarget.value

        this.setState({
            noteObjective: note
        })
    }

    handleSubmit(event: React.FormEvent<HTMLInputElement>) {
        event.preventDefault()

        if (this.state.percentObjective > 0 && this.state.percentObjective <= 100) {
            this.props.submitForm(this.state.percentObjective, this.state.noteObjective)
        } else {
            this.setState({
                fieldValid: false
            })
        }
    }

    render() {
        return (
            <Popin title="Create a objectif">
                <FormGroup onSubmit={this.handleSubmit.bind(this)}>
                    <InputLabel
                        idControl="control"
                        inputType="number"
                        inputPlaceholder="...%"
                        componentClass="mb-20"
                        onChange={this.valuePercentObjective.bind(this)}
                        inputValid={this.state.fieldValid}
                        warningMessage="The number must be between 0 and 100%"
                    >
                        Objective progress
                    </InputLabel>

                    <TextareaLabel idControl="note" onChange={this.valueNoteObjective.bind(this)}>
                        Note
                    </TextareaLabel>

                    <div className="popin__callAction d-flex justify-content-end">
                        <ButtonSecondary
                            size="normal"
                            status="danger"
                            classComponent="popin__cancel mr-20"
                            onClick={this.props.closePopin}
                        >
                            Cancel
                        </ButtonSecondary>
                        <ButtonPrimary type="submit" size="normal" status="success">
                            Yes, apply
                        </ButtonPrimary>
                    </div>
                </FormGroup>
            </Popin>
        )
    }
}

export { PopinObjective }
