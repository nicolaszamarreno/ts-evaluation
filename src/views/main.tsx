import * as React from "react"
import ReactCSSTransitionGroup = require("react-addons-css-transition-group")
import { Router } from "../router"
import { withRouter } from "react-router"

class Main extends React.Component<{}, {}> {
    render() {
        return (
            <main className="wrapper wrapper--no-shadow">
                <Router />
            </main>
        )
    }
}

export { Main }
