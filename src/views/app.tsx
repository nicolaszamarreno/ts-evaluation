import * as React from "react"
import { Breadcrumb } from "../components/organisms/breadcrumb"
import { Header, ItemMenu } from "../components/organisms/header"
import { Main } from "./main"
import { MenuActions } from "../components/organisms/menu-actions"
import { configRouterLinks } from "../router"
import { ButtonRound } from "../components/molecules/button-round"
import { Icon } from "../components/atoms/Icon"
import { Loader } from "../components/atoms/Loader"
import { ButtonMenu } from "../components/molecules/button-menu"
import { observer, inject } from "mobx-react"
import { ObjectivesStore } from "../store"
import { withRouter, RouteComponentProps } from "react-router-dom"

export interface AppProps extends RouteComponentProps<{}> {
    datas?: ObjectivesStore
}

export interface AppState {
    isLoading: boolean
}

@inject("datas")
@observer
class AppContainer extends React.Component<AppProps, AppState> {
    constructor(props: AppProps) {
        super(props)

        this.state = {
            isLoading: false
        }
    }

    componentWillMount() {
        this.props.datas!.fetchApp().then(response => {
            this.setState({
                isLoading: true
            })
        })
    }

    render() {
        if (this.state.isLoading) {
            return (
                <div id="root">
                    <Breadcrumb />
                    <div className="wrapper" id="main">
                        <Header
                            name={this.props.datas!.individual.name}
                            status={this.props.datas!.individual.email}
                            menuItems={configRouterLinks}
                        >
                            <MenuActions classComponent="navbar-profil__actions">
                                <li>
                                    <ButtonRound
                                        classComponent="menu-actions__action"
                                        borderColor="border-color--smart"
                                        color="color--smart"
                                    >
                                        <Icon icon="phone" />
                                    </ButtonRound>
                                </li>
                                <li>
                                    <ButtonRound
                                        classComponent="menu-actions__action"
                                        borderColor="border-color--smart"
                                        color="color--smart"
                                    >
                                        <Icon icon="mail2" />
                                    </ButtonRound>
                                </li>
                                <ButtonMenu
                                    rootContextId="root"
                                    label="Actions"
                                    iconClass="icon-add"
                                    isMainMenu={true}
                                    contextClass="menu-actions__menu"
                                    actionsListWithIcons={[
                                        {
                                            iconClass: "icon-add",
                                            label: "Create an objective",
                                            onClick: () => console.log("Hello")
                                        },
                                        {
                                            iconClass: "icon-edit",
                                            label: "Create a team objective",
                                            color: "color--smart",
                                            onClick: () => console.log("Hello")
                                        },
                                        {
                                            iconClass: "icon-add",
                                            label: "Create a form",
                                            color: "color--ruban",
                                            onClick: () => console.log("Hello")
                                        }
                                    ]}
                                    actionsListWithoutIcons={[]}
                                />
                            </MenuActions>
                        </Header>
                        <Main />
                    </div>
                </div>
            )
        } else {
            return <Loader />
        }
    }
}

export const App = withRouter(AppContainer)
