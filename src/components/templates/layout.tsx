import * as React from "react"
import { LayoutProps } from "../../interfaces/templates/layout-props"

class Layout extends React.Component<LayoutProps, {}> {
    constructor(props: LayoutProps) {
        super(props)
    }

    render() {
        return <div className={this.props.classModifier}>{this.props.children}</div>
    }
}

export { Layout }
