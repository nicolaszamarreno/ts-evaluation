import * as React from "react"
import { LayoutProps } from "../../interfaces/templates/layout-props"

class Container extends React.Component<LayoutProps, {}> {
    constructor(props: LayoutProps) {
        super(props)
    }

    render() {
        return (
            <div className={`container ${this.props.classModifier ? this.props.classModifier : ""}`}>{this.props.children}</div>
        )
    }
}

export { Container }
