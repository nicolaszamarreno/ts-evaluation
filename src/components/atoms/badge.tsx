import * as React from "react"

export interface BadgeProps {
    type: string
    value?: number
    classComponent?: string
    color?: string
    month?: string
}

class Badge extends React.Component<BadgeProps, {}> {
    render() {
        if (this.props.type === "header" && this.props.value != null) {
            const badgeColor = this.props.value > 2 ? "ruban" : "smart"

            return (
                <span className={`panel-header__badge badge badge--head background-color--${badgeColor}`}>
                    {this.props.value}
                </span>
            )
        } else if (this.props.type === "counter") {
            return <span className={`badge badge--head background-color--${this.props.color}`}>{this.props.value}</span>
        } else if (this.props.type === "date") {
            return (
                <span className="badge badge--date">
                    <strong>{this.props.value}</strong>
                    <span>{this.props.month}</span>
                </span>
            )
        }
    }
}

export { Badge }
