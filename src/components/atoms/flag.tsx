import * as React from "react"

export interface FlagProps {
    classComponent?: string
    classModifier: string
    flagPicture?: string
}

class Flag extends React.Component<FlagProps, {}> {
    constructor(props: FlagProps) {
        super(props)
    }

    render() {
        return (
            <div
                className={`${this.props.classComponent} flag flag--${this.props.classModifier}`}
                style={{ backgroundImage: `url(${this.props.flagPicture})` }}
            />
        )
    }
}

export { Flag }
