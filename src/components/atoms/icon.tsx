import * as React from "react"

interface IconProps {
    icon: string
    classComponent?: string
}

class Icon extends React.Component<IconProps, {}> {
    constructor(props: IconProps) {
        super(props)
    }

    render() {
        return <i className={`icon-${this.props.icon} ${this.props.classComponent ? this.props.classComponent : ""}`} />
    }
}

export { Icon }
