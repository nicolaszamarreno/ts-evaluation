import * as React from "react"

interface TextareaProps {
    componentClass?: string
    idControl: string
    name: string
    placeholder?: string
    onChange: () => void
}

class Textarea extends React.Component<TextareaProps, {}> {
    constructor(props: TextareaProps) {
        super(props)
    }

    render() {
        return (
            <textarea
                className={this.props.componentClass}
                name={this.props.idControl}
                id={this.props.idControl}
                placeholder={this.props.placeholder}
                onChange={this.props.onChange}
            />
        )
    }
}

export { Textarea }
