import * as React from "react"
import ChartStatClass from "../../assets/js/chart-stat-class"

export interface ChartStatProps {
    reRender?: boolean
    percent: number
    heightContainer?: number
    heightBar?: number
    paddingCornerRound?: number
    speed?: number
    fontSize?: number
    displayLegend?: boolean
    colorsStates: boolean
}

export interface ChartStatClassClass {
    update: (datas: number) => void
}

class ChartStat extends React.Component<ChartStatProps, {}> {
    private $chartStat: HTMLCanvasElement
    private chartGraph: ChartStatClassClass

    constructor(props: ChartStatProps) {
        super(props)
    }

    render() {
        return <canvas className="chart-stat" ref={(item: HTMLCanvasElement) => (this.$chartStat = item)} />
    }

    componentDidMount() {
        this.chartGraph = new ChartStatClass(this.$chartStat, {
            percent: this.props.percent,
            colorsStates: this.props.colorsStates
        })
    }

    componentDidUpdate() {
        if (this.props.reRender) {
            this.chartGraph.update(this.props.percent)
        }
    }
}

export { ChartStat }
