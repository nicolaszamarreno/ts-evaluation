import * as React from "react"

type ButtonTypes = "default" | "success" | "danger" | "warning" | "disable"
type ButtonSize = "small" | "medium" | "normal"

export interface ButtonPrimaryProps {
    size: ButtonSize
    status: ButtonTypes
    icon?: string
    type?: string
    classComponent?: string
    onClick?: () => void
}

class ButtonPrimary extends React.Component<ButtonPrimaryProps, {}> {
    constructor(props: ButtonPrimaryProps) {
        super(props)
    }
    render() {
        return (
            <button
                type={this.props.type}
                role="button"
                onClick={this.props.onClick}
                className={`button button-primary button-primary--${this.props.size} button-primary--${this.props.status} ${
                    this.props.classComponent
                    }`}
            >
                {this.props.icon && <i className={`button__icon ${this.props.icon}`} />}
                {this.props.children}
            </button>
        )
    }
}

export { ButtonPrimary }
