import * as React from "react"

type ButtonTypes = "default" | "success" | "danger" | "warning" | "disable"
type ButtonSize = "small" | "medium" | "normal"

export interface ButtonSecondProps {
    size: ButtonSize
    status: ButtonTypes
    icon?: string
    classComponent?: string
    onClick?: () => void
}

class ButtonSecondary extends React.Component<ButtonSecondProps, {}> {
    constructor(props: ButtonSecondProps) {
        super(props)
    }

    render() {
        return (
            <button
                role="button"
                onClick={this.props.onClick}
                className={`button button-secondary button-secondary--${this.props.size} button-secondary--${this.props.status} ${
                    this.props.classComponent
                    }`}
            >
                {this.props.icon && <i className={`button__icon ${this.props.icon}`} />}
                {this.props.children}
            </button>
        )
    }
}

export { ButtonSecondary }
