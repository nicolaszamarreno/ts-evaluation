import * as React from "react"
import { ChartBarClass } from "../../assets/js/chart-bar-class"
import { CoordinateBar, DatasReceive, OptionsChart } from "../../interfaces/atoms/chart-bar"

export interface ChartBarProps {
    reRender?: boolean
    datas: DatasReceive[]
    heightChart: number
    getIndexBar: (indexBar: number | null) => void
}

export interface ChartBarImportClass {
    setElements: () => CoordinateBar[]
    update: (datas: DatasReceive[]) => void
}

class ChartBar extends React.Component<ChartBarProps, {}> {
    private $chart: HTMLCanvasElement
    private chartGraph: ChartBarImportClass

    constructor(props: ChartBarProps) {
        super(props)
    }

    render() {
        return <canvas className="chart-bar" ref={(item: HTMLCanvasElement) => (this.$chart = item as HTMLCanvasElement)} />
    }

    componentDidMount() {
        this.chartGraph = new ChartBarClass(this.$chart, {
            height: this.props.heightChart,
            datas: this.props.datas
        })

        if (this.props.getIndexBar) {
            this.$chart.addEventListener("click", e => {
                let clickX = e.clientX - this.$chart.getBoundingClientRect().left
                let clickY = e.clientY - this.$chart.getBoundingClientRect().top

                const elements = this.chartGraph.setElements()
                let indexBarSelected = null

                for (let i = 0; i < elements.length; i++) {
                    if (
                        clickX > elements[i].startX &&
                        clickX < elements[i].endX &&
                        clickY < elements[i].startY &&
                        clickY > elements[i].endY
                    ) {
                        indexBarSelected = i
                    } else {
                        indexBarSelected
                    }
                }
                this.props.getIndexBar(indexBarSelected)
            })
        }
    }

    componentDidUpdate() {
        if (this.props.reRender) {
            this.chartGraph.update(this.props.datas)
        }
    }
}

export { ChartBar }
