import * as React from "react"

type statusType = "connect" | "candidate" | "collaborator" | "disable"

export interface AvatarProps {
    type: string
    status: statusType
    picture?: string
    classComponent?: string
}

class Avatar extends React.Component<AvatarProps, {}> {
    constructor(props: AvatarProps) {
        super(props)
    }

    render() {
        return this.getProfilType()
    }

    getProfilType() {
        if (this.props.type === "profil") {
            return (
                <div
                    className={`${this.props.classComponent ? this.props.classComponent : ""} avatar avatar--profil avatar--${
                        this.props.status
                    }`}
                    style={{ backgroundImage: `url(${this.props.picture})` }}
                >
                    {this.props.children}
                </div>
            )
        } else if (this.props.type === "profilmini") {
            return (
                <div
                    className={`${
                        this.props.classComponent ? this.props.classComponent : ""
                    } avatar avatar--profil-mini avatar--${this.props.status}`}
                    style={{ backgroundImage: `url(${this.props.picture})` }}
                >
                    {this.props.children}
                </div>
            )
        } else if (this.props.type === "header") {
            return (
                <div
                    className={`${this.props.classComponent ? this.props.classComponent : ""} avatar avatar--header avatar--${
                        this.props.status
                    }`}
                    style={{ backgroundImage: `url(${this.props.picture})` }}
                >
                    {this.props.children}
                </div>
            )
        } else if (this.props.type === "widget") {
            return (
                <div
                    className={`${this.props.classComponent ? this.props.classComponent : ""} avatar avatar--widget avatar--${
                        this.props.status
                    }`}
                    style={{ backgroundImage: `url(${this.props.picture})` }}
                >
                    {this.props.children}
                </div>
            )
        } else if (this.props.type === "feed") {
            return (
                <div
                    className={`${this.props.classComponent ? this.props.classComponent : ""} avatar avatar--feed avatar--${
                        this.props.status
                    }`}
                    style={{ backgroundImage: `url(${this.props.picture})` }}
                >
                    {this.props.children}
                </div>
            )
        }
    }
}

export { Avatar }
