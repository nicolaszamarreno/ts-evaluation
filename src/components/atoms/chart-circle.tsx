import * as React from "react"
import { ChartCircleClass } from "../../assets/js/chart-circle-class"

export interface ChartCircleProps {
    size: number
    percent: number
    strokeSize: number
    startDegrees?: number
    animOnScroll?: boolean
    colorsStates?: boolean
    legend?: HTMLElement | string
}

class ChartCircle extends React.Component<ChartCircleProps, {}> {
    private $refChart: HTMLCanvasElement
    private chart: ChartCircleClass

    constructor(props: ChartCircleProps) {
        super(props)
    }

    componentDidMount() {
        this.chart = new ChartCircleClass(this.$refChart, {
            strokeSize: this.props.strokeSize,
            size: this.props.size,
            animOnScroll: false
        })
    }

    componentDidUpdate() {
        //this.chart.update(this.props.percent)
    }

    render() {
        return (
            <canvas
                className="chart-circle__chart"
                width={this.props.size}
                height={this.props.size}
                data-percent={this.props.percent}
                ref={(chart: HTMLCanvasElement) => (this.$refChart = chart)}
            />
        )
    }
}

export { ChartCircle }
