import * as React from "react"

export interface TagProps {
    colorBackground?: string
    colorText?: string
    state?: string
    modifierClass?: string
}

class Tag extends React.Component<TagProps, {}> {
    constructor(props: TagProps) {
        super(props)
    }

    render() {
        return (
            <span
                className={`${this.props.modifierClass ? this.props.modifierClass : ""} tag ${
                    this.props.colorBackground ? this.props.colorBackground : ""
                } ${this.props.colorText ? this.props.colorText : ""}`}
            >
                {this.props.children}
            </span>
        )
    }
}

export { Tag }
