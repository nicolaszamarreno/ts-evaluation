import * as React from "react"

export interface LabelProps {
    idControl: string
    componentClass?: string
}

class Label extends React.Component<LabelProps, {}> {
    render() {
        return (
            <label className={`label ${this.props.componentClass}`} htmlFor={this.props.idControl}>
                {this.props.children}
            </label>
        )
    }
}

export { Label }
