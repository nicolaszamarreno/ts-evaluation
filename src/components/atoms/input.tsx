import * as React from "react"

type InputTypes = "text" | "number" | "mail"

interface InputProps {
    type: InputTypes
    pattern?: string
    componentClass?: string
    idControl?: string
    placeholder?: string
    value?: string
    required?: boolean
    onChange: () => void
    inputValid?: boolean
    warningMessage?: string
}

class Input extends React.Component<InputProps, {}> {
    constructor(props: InputProps) {
        super(props)
    }

    renderMessage() {}

    render() {
        return (
            <div className="input-text__box">
                <div className="input-text__wrapper">
                    <input
                        className={`input-text ${!this.props.inputValid ? "input-text--warning" : ""}`}
                        type={this.props.type}
                        id={this.props.idControl}
                        placeholder={this.props.placeholder}
                        value={this.props.value}
                        required={this.props.required}
                        name={this.props.idControl}
                        pattern={this.props.pattern}
                        onChange={this.props.onChange}
                    />
                    <span className="input-text__status-bar" />
                </div>
                {!this.props.inputValid && <span className="input__message">{this.props.warningMessage}</span>}
            </div>
        )
    }
}

export { Input }
