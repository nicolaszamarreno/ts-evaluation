import * as React from "react"

export interface LoaderProps {}

class Loader extends React.Component<LoaderProps, {}> {
    constructor(props: LoaderProps) {
        super(props)
    }

    render() {
        return (
            <div className="loader">
                <svg viewBox="0 0 64 64">
                    <g strokeWidth="7" strokeLinecap="round">
                        <line x1="10" x2="10" y2="44.8133" y1="19.1867">
                            <animate attributeName="y1" dur="750ms" values="16;18;28;18;16;16" repeatCount="indefinite" />
                            <animate attributeName="y2" dur="750ms" values="48;46;36;44;48;48" repeatCount="indefinite" />
                            <animate
                                attributeName="stroke-opacity"
                                dur="750ms"
                                values="1;.4;.5;.8;1;1"
                                repeatCount="indefinite"
                            />
                        </line>
                        <line x1="24" x2="24" y2="47.7627" y1="16.2373">
                            <animate attributeName="y1" dur="750ms" values="16;16;18;28;18;16" repeatCount="indefinite" />
                            <animate attributeName="y2" dur="750ms" values="48;48;46;36;44;48" repeatCount="indefinite" />
                            <animate
                                attributeName="stroke-opacity"
                                dur="750ms"
                                values="1;1;.4;.5;.8;1"
                                repeatCount="indefinite"
                            />
                        </line>
                        <line x1="38" x2="38" y2="48" y1="16">
                            <animate attributeName="y1" dur="750ms" values="18;16;16;18;28;18" repeatCount="indefinite" />
                            <animate attributeName="y2" dur="750ms" values="44;48;48;46;36;44" repeatCount="indefinite" />
                            <animate
                                attributeName="stroke-opacity"
                                dur="750ms"
                                values=".8;1;1;.4;.5;.8"
                                repeatCount="indefinite"
                            />
                        </line>
                        <line x1="52" x2="52" y2="44.4747" y1="17.7627">
                            <animate attributeName="y1" dur="750ms" values="28;18;16;16;18;28" repeatCount="indefinite" />
                            <animate attributeName="y2" dur="750ms" values="36;44;48;48;46;36" repeatCount="indefinite" />
                            <animate
                                attributeName="stroke-opacity"
                                dur="750ms"
                                values=".5;.8;1;1;.4;.5"
                                repeatCount="indefinite"
                            />
                        </line>
                    </g>
                </svg>
            </div>
        )
    }
}

export { Loader }
