import * as React from "react"
import { NavbarHorizontalClass } from "../../assets/js/navbar-horizontal-navigation"

export interface NavbarHorizontalProps {
    elementsMenu?: object[]
}

class NavbarHorizontal extends React.Component<NavbarHorizontalProps, {}> {
    constructor(props: NavbarHorizontalProps) {
        super(props)
    }

    componentDidMount() {
        new NavbarHorizontalClass()
    }

    render() {
        return (
            <nav className="navbar" role="navigation">
                <span className="navbar-status navbar-status--horizontal" />
                <ul className="navbar--horizontal">{this.props.children}</ul>
            </nav>
        )
    }
}

export { NavbarHorizontal }
