import * as React from "react"

export interface ButtonRoundProps {
    classComponent?: string
    attribute?: string
    bgColor?: string
    borderColor?: string
    color?: string
    onClick?: () => void
}

class ButtonRound extends React.Component<ButtonRoundProps, {}> {
    constructor(props: ButtonRoundProps) {
        super(props)
    }

    getProperties(): string[] {
        const properties = []

        this.props.classComponent ? properties.push(this.props.classComponent) : ""
        this.props.bgColor ? properties.push(this.props.bgColor) : ""
        this.props.borderColor ? properties.push(this.props.borderColor) : ""
        this.props.color ? properties.push(this.props.color) : ""

        return properties
    }

    render() {
        return (
            <button role="button" className={`button button-round ${this.getProperties().join(" ")}`}>
                {this.props.children}
            </button>
        )
    }
}

export { ButtonRound }
