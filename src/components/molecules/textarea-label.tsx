import * as React from "react"
import { Label } from "../atoms/label"
import { Textarea } from "../atoms/textarea"

interface TextareaLabelProps {
    inline?: boolean
    idControl: string
    classComponent?: string
    TextareaClassName?: string
    TextareaPlaceholder?: string
    onChange: () => void
}

class TextareaLabel extends React.Component<TextareaLabelProps, {}> {
    constructor(props: TextareaLabelProps) {
        super(props)
    }

    render() {
        return (
            <div className={`d-flex ${this.props.classComponent} ${this.props.inline ? "flex-row" : "flex-column"}`}>
                <Label idControl={this.props.idControl}>{this.props.children}</Label>
                <Textarea
                    componentClass={this.props.TextareaClassName}
                    idControl={this.props.idControl}
                    placeholder={this.props.TextareaPlaceholder}
                    name={this.props.idControl}
                    onChange={this.props.onChange}
                />
            </div>
        )
    }
}

export { TextareaLabel }
