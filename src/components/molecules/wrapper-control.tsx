import * as React from "react"
import { Collapse } from "../../assets/js/collapse"

export interface WrapperControlProps {
    idWrapper: string
}

class WrapperControl extends React.Component<WrapperControlProps, {}> {
    private $controller: HTMLDivElement
    private actionCollapse: any

    constructor(props: WrapperControlProps) {
        super(props)
    }

    componentDidMount() {
        this.actionCollapse = new Collapse({
            buttonCollapse: this.$controller,
            contentWrapper: `${this.props.idWrapper} .wrapper-collapse__content`,
            time: ".5",
            idWrapper: this.props.idWrapper
        })
    }

    handleClick() {
        this.actionCollapse.collapse()
    }

    render() {
        return (
            <div
                className="wrapper-collapse__controller"
                onClick={() => this.handleClick()}
                ref={element => (this.$controller = element!)}
            >
                {this.props.children}
            </div>
        )
    }
}

export { WrapperControl }
