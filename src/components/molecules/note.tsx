import * as React from "react"

export interface NoteProps {
    title: string
    icon: string
    score: string | number
    numberIcons?: number
}

class Note extends React.Component<NoteProps, {}> {
    constructor(props: NoteProps) {
        super(props)
    }

    render() {
        return (
            <ul className="note">
                <li className="note__label">{this.props.title}</li>
                <li className="note__markers">
                    <div className="stat-icons">
                        <span className="stat-icons__ladder">{this.renderIcon()}</span>
                        <span className="stat-icons__ladder stat-icons__ladder--anim">{this.renderIcon()}</span>
                    </div>
                </li>
                <li className="note__score">
                    <span className="note__tag tag background-color--smart">{this.props.score}</span>
                </li>
            </ul>
        )
    }

    renderIcon() {
        const numberIcons = this.props.numberIcons ? this.props.numberIcons : 8

        const iconsElement: JSX.Element[] = []

        for (let icon = 0; icon < numberIcons; icon++) {
            iconsElement.push(<i key={icon} className={this.props.icon} />)
        }

        return iconsElement
    }
}

export { Note }
