import * as React from "react"
import { Avatar } from "../atoms/avatar"
import { Flag } from "../atoms/flag"

export interface NavbarProfilProps {
    name: string
    status: string
    picture?: string
    flag?: string
}

class NavbarProfil extends React.Component<NavbarProfilProps, {}> {
    constructor(props: NavbarProfilProps) {
        super(props)
    }

    render() {
        return (
            <div className="navbar-profil">
                <div className="sticky__navbar-profil navbar-profil__background">
                    <div className="navbar-profil__wrapper">
                        <div className="navbar-profil__identity">
                            <ul className="navbar-profil__identity-item navbar-list-profil">
                                <li className="navbar-list-profil__item">
                                    <h1 className="color--smart">{this.props.name}</h1>
                                    <em className="color--protoss navbar-list-profil__status">{this.props.status}</em>
                                </li>
                                <li className="navbar-list-profil__item navbar-miniCard-profil">
                                    <div className="avatar avatar--header avatar--collaborator navbar-miniCard-profil__avatar">
                                        <div className="flag flag--header navbar-miniCard-profil__flag" />
                                    </div>
                                    <h1 className="color--smart">{this.props.name}</h1>
                                </li>
                            </ul>
                        </div>
                        <div className="navbar-profil__avatar">
                            <Avatar type="profil" status="connect" />
                            <Flag classComponent="navbar-profil__flag" classModifier="md" />
                        </div>
                        {this.props.children}
                    </div>
                </div>
            </div>
        )
    }
}

export { NavbarProfil }
