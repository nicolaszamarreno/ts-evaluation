import * as React from "react"
import * as ReactDOM from "react-dom"
import { initializeMenuList } from "../../assets/js/menu"

export interface ListButtonProps {
    label: string
    iconClass: string
    contextClass: string
    isMainMenu: boolean
    rootContextId: string
    actionsListWithIcons?: ButtonPropsIcons[]
    actionsListWithoutIcons?: ButtonProps[]
}

export interface ButtonPropsIcons {
    iconClass: string
    color?: string
    label: string
    onClick: () => any
}

export interface ButtonProps {
    label: string
    onClick: () => any
}

class ButtonMenu extends React.Component<ListButtonProps, {}> {
    private menuButton: HTMLButtonElement
    private menuList: HTMLElement

    componentDidMount() {
        const rootElement = document.getElementById(this.props.rootContextId)
        if (rootElement && this.menuList) {
            rootElement.appendChild(this.menuList)
            initializeMenuList(this.menuButton, this.menuList, this.props.isMainMenu)
        }
    }

    render() {
        return (
            <button
                className={`${
                    this.props.contextClass
                } button button-round background-color--smart border-color--smart color--stormtrooper`}
                title={this.props.label}
                ref={button => (this.menuButton = button!)}
            >
                <i className={this.props.iconClass} />
                {this.renderList()}
            </button>
        )
    }

    renderList() {
        if (
            (this.props.actionsListWithIcons && this.props.actionsListWithIcons.length > 0) ||
            (this.props.actionsListWithoutIcons && this.props.actionsListWithoutIcons.length)
        ) {
            const buttonElements: JSX.Element[] = []
            const buttonWiElements: JSX.Element[] = []

            if (this.props.actionsListWithIcons) {
                for (const button of this.props.actionsListWithIcons) {
                    buttonElements.push(
                        <li
                            className={`panel-menu__item ${button.color ? button.color : ""}`}
                            key={button.label}
                            title={button.label}
                            onClick={button.onClick}
                        >
                            <i className={`panel-menu__icon ${button.iconClass}`} />
                            <a>{button.label}</a>
                        </li>
                    )
                }
            }

            if (this.props.actionsListWithoutIcons) {
                for (const button of this.props.actionsListWithoutIcons) {
                    buttonWiElements.push(
                        <li className="panel-menu__item" key={button.label} title={button.label} onClick={button.onClick}>
                            <a>{button.label}</a>
                        </li>
                    )
                }
            }

            return (
                <nav
                    className={`panel-menu ${this.props.isMainMenu ? "panel-menu--main" : ""}`}
                    style={{ zIndex: 1500 }}
                    ref={nav => (this.menuList = nav!)}
                >
                    <span className="panel-menu__marker" />
                    {buttonElements.length > 0 ? <ul className="panel-menu__list">{buttonElements}</ul> : null}
                    {buttonWiElements.length > 0 ? (
                        <div className="panel-menu__list panel-menu__list--scroll">
                            <ul className="panel-menu__wrapper scrollbar--active ps ps--active-y">{buttonWiElements}</ul>
                        </div>
                    ) : null}
                </nav>
            )
        }
    }
}

export { ButtonMenu }
