import * as React from "react"
import { Avatar } from "../atoms/avatar"

type statutType = "connect" | "candidate" | "collaborator" | "disable"

export interface FeedProps {
    typeAvatar: string
    status: statutType
    picture?: string
    classComponent?: string
}

class Feed extends React.Component<FeedProps, {}> {
    constructor(props: FeedProps) {
        super(props)
    }

    render() {
        return (
            <div className={`feed ${this.props.classComponent ? this.props.classComponent : ""}`}>
                <div className="feed__avatar">
                    <Avatar type={this.props.typeAvatar} status={this.props.status} picture={this.props.picture} />
                </div>
                <div className="feed__desc">
                    <p>{this.props.children}</p>
                </div>
            </div>
        )
    }
}

export { Feed }
