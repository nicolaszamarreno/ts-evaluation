import * as React from "react"
import { Badge } from "../atoms/badge"

export interface PanelHeaderProps {
    title: string
    badgeValue?: number
    headerElements?: React.ReactNode
}

class PanelHeader extends React.Component<PanelHeaderProps, {}> {
    constructor(props: PanelHeaderProps) {
        super(props)
    }

    render() {
        return (
            <header className="panel-header justify-content-between">
                <h2>
                    <Badge type="header" value={this.props.badgeValue} />
                    {this.props.title}
                </h2>
                {this.props.headerElements}
            </header>
        )
    }
}

export { PanelHeader }
