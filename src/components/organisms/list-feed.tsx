import * as React from "react"
import { Feed } from "../molecules/feed"

type statutType = "connect" | "candidate" | "collaborator" | "disable"

export interface ListFeeds {
    status: statutType
    typeAvatar: string
    picture?: string
}

export interface ListFeedProps {
    listFeeds: ListFeeds[]
    customClass?: string
    typeAvatar: string
}

class ListFeed extends React.Component<ListFeedProps, {}> {
    constructor(props: ListFeedProps) {
        super(props)
    }

    render() {
        return <ul className={`list-feed ${this.props.customClass}`}>{this.renderFeeds()}</ul>
    }

    renderFeeds() {
        const listFeed = this.props.listFeeds.map((item: ListFeeds, index: number) => {
            return (
                <li key="index" className="list-feed__item">
                    <Feed status={item.status} picture={item.picture} typeAvatar={this.props.typeAvatar}>
                        {this.props.children}
                    </Feed>
                </li>
            )
        })

        return listFeed
    }
}

export { ListFeed }
