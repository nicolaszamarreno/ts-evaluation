import * as React from "react"

export interface FormGroupProps {
    onSubmit: () => void
}

class FormGroup extends React.Component<FormGroupProps, {}> {
    constructor(props: FormGroupProps) {
        super(props)
    }

    render() {
        return (
            <form className="form" onSubmit={this.props.onSubmit}>
                {this.props.children}
            </form>
        )
    }
}

export { FormGroup }
