import * as React from "react"

export interface WrapperCollapseProps {
    id: string
}

class WrapperCollapse extends React.Component<WrapperCollapseProps, {}> {
    constructor(props: WrapperCollapseProps) {
        super(props)
    }

    render() {
        return (
            <div className="wrapper-collapse" id={this.props.id}>
                <div className="wrapper-collapse__content">{this.props.children}</div>
            </div>
        )
    }
}

export { WrapperCollapse }
