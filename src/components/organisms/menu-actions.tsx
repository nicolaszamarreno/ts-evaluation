import * as React from "react"
import { Icon } from "../atoms/Icon"

export interface MenuActionsProps {
    classComponent?: string
    menuConfig?: ConfigButton
}

export interface ConfigButton {
    classComponent?: string
    borderColor?: string
    color?: string
    icon?: string
}

class MenuActions extends React.Component<MenuActionsProps, {}> {
    constructor(props: MenuActionsProps) {
        super(props)
    }

    render() {
        return <ul className={`${this.props.classComponent} menu-actions`}>{this.props.children}</ul>
    }
}

export { MenuActions }
