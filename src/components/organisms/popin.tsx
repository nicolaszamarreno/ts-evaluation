import * as React from "react"
import { ButtonPrimary } from "../atoms/button-primary"

export interface PopinProps {
    instruction?: string
    title: string
}

class Popin extends React.Component<PopinProps, {}> {
    constructor(props: PopinProps) {
        super(props)
    }

    render() {
        return (
            <div className="popin__overlaid">
                <div className="popin__wrapper popin__wrapper--enter">
                    <h1 className="popin__title color--smart">{this.props.title}</h1>
                    {this.props.instruction && <p className="popin__instruction">{this.props.instruction}</p>}
                    {this.props.children}
                </div>
            </div>
        )
    }
}

export { Popin }
