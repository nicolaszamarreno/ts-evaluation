import * as React from "react"
import { Sticky } from "../../assets/js/sticky"
import { NavbarProfil } from "../molecules/navbar-profil"
import { NavbarHorizontal } from "../../components/molecules/navbar-horizontal"
import { NavLink } from "react-router-dom"

export interface ItemMenu {
    name: string
    path: string
}

export interface HeaderProps {
    name: string
    status: string
    pictureAvatar?: string
    menuItems?: ItemMenu[]
}

class Header extends React.Component<HeaderProps, {}> {
    constructor(props: HeaderProps) {
        super(props)
    }

    componentDidMount() {
        const $menuFixed = document.querySelector(".navbar-profil") as HTMLElement
        const $whereAddClass = document.querySelector(".sticky__navbar-profil") as HTMLElement
        new Sticky($menuFixed, {
            className: "navbar-profil--sticky",
            elementAddClass: $whereAddClass
        })
    }

    render() {
        return (
            <header className="header">
                <NavbarProfil name={this.props.name} status={this.props.status}>
                    {this.props.children}
                </NavbarProfil>
                {this.props.menuItems && <div className="row header__navbar">{this.renderMenu()}</div>}
            </header>
        )
    }

    renderMenu() {
        if (this.props.menuItems) {
            const MenuHorizontal = this.props.menuItems.map((item, index) => {
                return (
                    <li key={index}>
                        <NavLink exact to={item.path} activeClassName="navbar--horizontal--active">
                            {item.name}
                        </NavLink>
                    </li>
                )
            })

            return <NavbarHorizontal>{MenuHorizontal}</NavbarHorizontal>
        }
    }
}

export { Header }
