import * as React from "react"
import { Note } from "../molecules/note"

export interface Notes {
    title: string
    icon: string
    score: number
}

export interface ListNotesProps {
    notes: Notes[]
}

class ListNotes extends React.Component<ListNotesProps, {}> {
    constructor(props: ListNotesProps) {
        super(props)
    }

    render() {
        return (
            <ul className="list-notes">
                {this.renderNote()}

                <li className="list-notes__item">
                    <ul className="note">
                        <li className="note__label">Type</li>
                        <li className="note__markers">
                            <span className="note__tag tag background-color--smart">Personal</span>
                            <span className="note__tag tag background-color--smart">Team</span>
                            <span className="note__tag tag background-color--smart">Values</span>
                        </li>
                    </ul>
                </li>
            </ul>
        )
    }

    renderNote() {
        const notes: JSX.Element[] = []

        for (let index = 0; index < this.props.notes.length; index++) {
            notes.push(
                <li key={index} className="list-notes__item">
                    <Note
                        title={this.props.notes[index].title}
                        icon={this.props.notes[index].icon}
                        score={this.props.notes[index].score}
                    />
                </li>
            )
        }

        return notes
    }
}

export { ListNotes }
