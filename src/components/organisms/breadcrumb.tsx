import * as React from "react"

export interface BreadcrumbProps {
    name?: string
}

class Breadcrumb extends React.Component<BreadcrumbProps, {}> {
    render() {
        return (
            <nav className="breadcrumb">
                <a href="#" className="breadcrumb__return icon-left" />
                <a href="#" className="breadcrumb__wire">
                    ...
                </a>
                <i className="breadcrumb__next icon-right" />
                <a href="#" className="breadcrumb__wire">
                    Vacancy application : Assistant ...
                </a>
                <i className="breadcrumb__next icon-right" />
                <span className="breadcrumb__wire breadcrumb__wire--selected">Profil de {this.props.name}</span>
            </nav>
        )
    }
}

export { Breadcrumb }
