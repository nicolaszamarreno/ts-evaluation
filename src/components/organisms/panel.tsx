import * as React from "react"

export interface PanelProps {
    id?: string
    badgeValue?: number
    badgeClick?: () => void
    badgeTooltip?: string
    headerElements?: React.ReactNode
}

class Panel extends React.Component<PanelProps, {}> {
    render() {
        return (
            <section id={this.props.id} className="panel">
                {this.props.children}
            </section>
        )
    }
}

export { Panel }
