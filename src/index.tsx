import * as React from "react"
import * as ReactDOM from "react-dom"
import createBrowserHistory from "history/createBrowserHistory"
import { Provider } from "mobx-react"
import * as mobx from "mobx"
import { RouterStore, syncHistoryWithStore } from "mobx-react-router"
import { Router } from "react-router"
import { App } from "./views/app"
import { ObjectivesStore } from "../src/store"

mobx.useStrict(true)

const browserHistory = createBrowserHistory()

const routingStore = new RouterStore()
const storeObjectives = new ObjectivesStore()

const stores = {
    routing: routingStore,
    datas: storeObjectives
}

const history = syncHistoryWithStore(browserHistory, routingStore)

ReactDOM.render(
    <Provider {...stores}>
        <Router history={history}>
            <App />
        </Router>
    </Provider>,
    document.querySelector("#app")
)
